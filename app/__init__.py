from flask import Flask
#from app import routes


def create_app():
  app = Flask(__name__)
  # Cria a instancia app

  with app.app_context():
    # Importa e registra rotas
    from . import routes

  return app
