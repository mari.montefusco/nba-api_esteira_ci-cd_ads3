# Imagem base do Python 
FROM python:3.9-slim

# Diretório do trabalho dentro do container
WORKDIR /app

# Copia o arquivo requirements.txt para o diretório de trabalho
COPY requirements.txt .

# Instala as dependências
RUN pip install --no-cache-dir -r requirements.txt

# Copia os arquivos p/ o diretório de trabalho
COPY . . 

# Porta q a aplicação vai rodar
EXPOSE 5001

# Comando p rodar 
CMD ["python", "run.py"]