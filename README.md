# Projeto API NBA

Esta é uma API simples construída com Flask que retorna em JSON o resultado de 3 jogos de basquete.
Foi um desafio proposto na faculdade com o objetivo de implementar uma esteira CI/CD, contendo os estágios Build, Test e Deploy.

## Instalação

1. Clone o repositório:
   
   ```sh
   git clone git@github.com:marimontefusco/nba-api_esteira_ci-cd_ads3.git

2. Entre no diretório do projeto:
   
   ```sh
   cd nba-api_esteira_ci-cd_ads3

## Ambiente Virtual
3. Crie e ative um ambiente virtual:
   
   ```sh
   python -m venv venv
   source venv/bin/activate # No Windows use `venv\Scripts\activate`

## Dependências: 
4. Instale as dependências:
   
   ```sh
   pip install -r requirements.txt

## Execução 
5. Rodar aplicação
   
   ```sh
   python run.py

## Testar API no navegador
http://127.0.01:5001/

## Acessar os dados em JSON
htttp://127.0.0.1.5001/v1/resultados_nba

